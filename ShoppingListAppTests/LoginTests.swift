//
//  LoginTests.swift
//  ShoppingListAppTests
//
//  Created by Irina Cercel on 01/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import XCTest
@testable import ShoppingListApp

var loginDidSuccedCalled = false
var loginDidFailCalled = false

class LoginTests: XCTestCase {
    
    var sut: LoginVM!

    override func setUp() {
        sut = LoginVM(Storage: StorageMock(), delegate: LoginViewMock())
    }

    override func tearDown() {
        sut = nil
    }
    
    func testIsUsernameValid() {
        sut.newUsername = TestData.mainUsersArray[0].getUsername()
        
        let validation = sut.isUsernameValid()
        
        XCTAssertTrue(validation)
    }
    
    func testIsPasswordValid() {
        sut.newPassword = TestData.mainUsersArray[0].getPassword()
        
        let validation = sut.isPasswordValid()
        
        XCTAssertTrue(validation)
    }
    
    func testUserIsFound() {
        sut.newUsername = TestData.mainUsersArray[0].getUsername()
        sut.newPassword = TestData.mainUsersArray[0].getPassword()

        let userFound = sut.findUser()
        
        XCTAssertNotNil(userFound)
    }
    
    func testLoginDidSucced() {
        sut.newUsername = TestData.mainUsersArray[0].getUsername()
        sut.newPassword = TestData.mainUsersArray[0].getPassword()
        sut.delegate = LoginViewMock()

        sut.login()
        
        XCTAssertTrue(loginDidSuccedCalled)
    }
    
    func testLoginDidFail() {
        sut.newUsername = "user5"
        sut.newPassword = "pass5"
        
        sut.login()
        
        XCTAssertTrue(loginDidFailCalled)
    }
}

class LoginViewMock: LoginVMDelegate {
    
    func loginDidSucced(user: User) {
        loginDidSuccedCalled = true
    }
    
    func loginDidFail() {
        loginDidFailCalled = true
    }
}

class StorageMock: StorageProtocol {
    var jsonFilePath: URL?
    
    func readJSONFromFile<T>() -> [T] where T : Decodable, T : Encodable {
        if T.self == User.self {
            print("user")
            return TestData.mainUsersArray as! [T]
        } else {
            print("item")
            return TestData.mainItemsArray as! [T]
        }
    }
    
    func writeToJSONFile<T>(user: T) where T : Decodable, T : Encodable {
        if T.self == User.self {
            TestData.mainUsersArray.append(user as! User)
        } else {
            TestData.mainItemsArray.append(user as! Item)
        }
    }
}
