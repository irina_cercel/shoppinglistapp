//
//  TestData.swift
//  ShoppingListAppTests
//
//  Created by Irina Cercel on 01/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import Foundation
@testable import ShoppingListApp

struct TestData {
    static var mainUsersArray = [ User(username: "user1", password: "pass1"),
                                  User(username: "user2", password: "pass2"),
                                  User(username: "user3", password: "pass3"),
                                  User(username: "user4", password: "pass4") ]
    static var mainItemsArray = [ Item(name: "item1", description: "desc1", user: User(username: "user1", password: "pass1")),
                                  Item(name: "item2", description: "desc2", user: User(username: "user1", password: "pass1")),
                                  Item(name: "item3", description: "desc3", user: User(username: "user2", password: "pass2")),
                                  Item(name: "item4", description: "desc4", user: User(username: "user3", password: "pass3")) ]
}
