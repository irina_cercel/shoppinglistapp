//
//  RegisterTests.swift
//  ShoppingListAppTests
//
//  Created by Irina Cercel on 01/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import XCTest
@testable import ShoppingListApp

var registerDidSuccedCalled = false
var invalidInfosCalled = false
var userExistsCalled = false

class RegisterTests: XCTestCase {
    
    var sut: RegisterVM!

    override func setUp() {
        sut = RegisterVM(Storage: StorageMock(), delegate: RegisterViewMock())
    }

    override func tearDown() {
        sut = nil
    }

    func testUsernameIsUpdated() {
        sut.updateUsername(updatedUsername: TestData.mainUsersArray[0].getUsername())
        
        XCTAssertNotNil(sut.getUsername())
        XCTAssertEqual(sut.getUsername(), TestData.mainUsersArray[0].getUsername())
    }
    
    func testPasswordIsUpdated() {
        sut.updatePassword(updatedPassword: TestData.mainUsersArray[0].getPassword())
        
        XCTAssertNotNil(sut.getPassword())
        XCTAssertEqual(sut.getPassword(), TestData.mainUsersArray[0].getPassword())
    }
    
    func testConfirmPasswordIsUpdated() {
        sut.updateConfirmPassword(updatedConfirmPassword: TestData.mainUsersArray[0].getPassword())
        
        XCTAssertNotNil(sut.getConfirmPassword())
        XCTAssertEqual(sut.getConfirmPassword(), TestData.mainUsersArray[0].getPassword())
    }
    
    func testIsUsernameValid() {
        sut.updateUsername(updatedUsername: TestData.mainUsersArray[0].getUsername())
        
        let validation = sut.isUsernameValid()
        
        XCTAssertTrue(validation)
    }
    
    func testIsPasswordValid() {
        sut.updatePassword(updatedPassword: TestData.mainUsersArray[0].getPassword())
        sut.updateConfirmPassword(updatedConfirmPassword: TestData.mainUsersArray[0].getPassword())

        let validation = sut.isPasswordValid()
        
        XCTAssertTrue(validation)
    }
    
    func testUserIsFound() {
        sut.updateUsername(updatedUsername: TestData.mainUsersArray[0].getUsername())

        let userFound = sut.findUser()
        
        XCTAssertNotNil(userFound)
    }
    
    func testRegisterDidSucced() {
        sut.updateUsername(updatedUsername: "user5")
        sut.updatePassword(updatedPassword: "pass5")
        sut.updateConfirmPassword(updatedConfirmPassword: "pass5")
        
        sut.register()
        
        XCTAssertTrue(registerDidSuccedCalled)
        
        var userIsRegistered = false
        for user in TestData.mainUsersArray {
            if userIsRegistered == false {
                if user.getUsername() == sut.getUsername() {
                    userIsRegistered = true
                }
            }
        }
        XCTAssertTrue(userIsRegistered)
    }
    
    func testRegisterInvalidInfos() {
       // sut.updateUsername(updatedUsername: "user5")
        sut.updatePassword(updatedPassword: "pass5")
        sut.updateConfirmPassword(updatedConfirmPassword: "pass5")
        
        sut.register()
        
        XCTAssertTrue(invalidInfosCalled)
        
        
    }
    
    func testRegisterUserExists() {
        sut.updateUsername(updatedUsername: TestData.mainUsersArray[0].getUsername())
        sut.updatePassword(updatedPassword: TestData.mainUsersArray[0].getPassword())
        sut.updateConfirmPassword(updatedConfirmPassword: TestData.mainUsersArray[0].getPassword())
        
        sut.register()
        
        XCTAssertTrue(userExistsCalled)
        
    }
    
}

class RegisterViewMock: RegisterProtocol {
    func registerDidSucced(user: User) {
        registerDidSuccedCalled = true
    }
    
    func invalidInfos() {
        invalidInfosCalled = true
    }
    
    func userExists() {
        userExistsCalled = true
    }
}
