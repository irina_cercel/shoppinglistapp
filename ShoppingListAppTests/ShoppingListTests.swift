//
//  ShoppingListTests.swift
//  ShoppingListAppTests
//
//  Created by Irina Cercel on 01/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import XCTest
@testable import ShoppingListApp

var addItemCalled = false

class ShoppingListTests: XCTestCase {
    
    var sut: ShoppingListVM!

    override func setUp() {
        sut = ShoppingListVM(Storage: StorageMock(), user: TestData.mainUsersArray[0], delegate: ShoppingListViewMock())
    }

    override func tearDown() {
        sut = nil
    }

    func testItemsSetForUser() {
        let expectedItemsArray = [ Item(name: "item1", description: "desc1", user: User(username: "user1", password: "pass1")),
                                   Item(name: "item2", description: "desc2", user: User(username: "user1", password: "pass1"))]
        
        sut.setItemsFromJSON()

        XCTAssertTrue(sut.getItemsArray().elementsEqual(expectedItemsArray, by: { return $0.getUser().getUsername() == $1.getUser().getUsername() }))
    }

    func testItemIsAdded() {
        sut.addItem()
        
        XCTAssertTrue(addItemCalled)
        
    }
}

class ShoppingListViewMock: ShoppingListDelegate {
    func addItem() {
        addItemCalled = true
    }
}
