//
//  AddItemTests.swift
//  ShoppingListAppTests
//
//  Created by Irina Cercel on 01/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import XCTest
@testable import ShoppingListApp

var addDidSuccedCalled = false
var addDidFailCalled = false

class AddItemTests: XCTestCase {
    
    var sut: AddItemVM!

    override func setUp() {
        sut = AddItemVM(Storage: StorageMock(), user: TestData.mainUsersArray[0], delegate: AddItemViewMock())
    }

    override func tearDown() {
        sut = nil
    }
    
    func testItemIsAdded() {
        sut.setItemName(name: "item4")
        sut.setItemDescription(description: "desc4")
        sut.addItem(user: sut.getUser())
        
        XCTAssertTrue(addDidSuccedCalled)
        
        var itemIsAdded = false
        for item in TestData.mainItemsArray {
            if itemIsAdded == false {
                if item.getName() == sut.getItemName() && item.getUser().getUsername() == sut.getUser().getUsername() {
                    itemIsAdded = true
                }
            }
        }
        
        XCTAssertTrue(itemIsAdded)
    }
}

class AddItemViewMock: AddItemDelegate {
    func addDidSucced() {
        addDidSuccedCalled = true
    }
    
    func addDidFail() {
        addDidFailCalled = true
    }
    
    
}
