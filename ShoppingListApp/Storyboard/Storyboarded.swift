//
//  Storyboarded.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 02/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import Foundation
import UIKit

protocol Storyboarded {
    static func instantiate() -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyoard.instantiateViewController(withIdentifier: id) as! Self
    }
    
}
