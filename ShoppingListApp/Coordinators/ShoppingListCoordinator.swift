//
//  ShoppingListCoordinator.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 02/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class ShoppingListCoordinator: Coordinator {
    var navigationController: UINavigationController
    
    var user: User
    
    init(navigationController: UINavigationController, user: User) {
        self.navigationController = navigationController
        self.user = user
    }
    
    func start() {
        let shoppingListView = ShoppingListView.instantiate()
        shoppingListView.shoppingListVM = ShoppingListVM(storage: Storage(), user: user, delegate: shoppingListView)
        shoppingListView.shoppingListVM?.coordinator = self
        navigationController.pushViewController(shoppingListView, animated: true)
    }
    
    func addItem() {
        let addItemCoordinator = AddItemCoordinator(navigationController: navigationController, user: user)
        addItemCoordinator.start()
    }
    
    func logOut() {
        navigationController.popViewController(animated: true)
    }
}
