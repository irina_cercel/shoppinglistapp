//
//  Coordinators.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 02/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//
import UIKit

protocol Coordinator: class {
    var navigationController: UINavigationController { get set }
    
    func start()
}
