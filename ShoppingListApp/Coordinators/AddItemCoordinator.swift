//
//  AddItemCoordinator.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 02/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class AddItemCoordinator: Coordinator {
    var navigationController: UINavigationController
    
    var user: User
    
    init(navigationController: UINavigationController, user: User) {
        self.navigationController = navigationController
        self.user = user
    }
    
    func start() {
        let addItemView = AddItemView.instantiate()
        addItemView.addItemVM = AddItemVM(storage: Storage(), user: user, delegate: addItemView)
        addItemView.addItemVM?.coordinator = self
        navigationController.pushViewController(addItemView, animated: true)
    }
    
    func submit() {
        navigationController.popViewController(animated: true)
    }
}

