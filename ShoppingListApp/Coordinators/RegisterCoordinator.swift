//
//  BasicCoordinator.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 02/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class RegisterCoordinator: Coordinator {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let registerView = RegisterView.instantiate()
        registerView.registerVM = RegisterVM(storage: Storage(), delegate: registerView)
        registerView.registerVM?.coordinator = self
        navigationController.pushViewController(registerView, animated: true)
    }
    
    func register() {
        navigationController.popViewController(animated: true)
    }
}
