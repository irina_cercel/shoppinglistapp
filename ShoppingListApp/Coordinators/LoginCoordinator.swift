//
//  LoginCoordinator.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 02/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class LoginCoordinator: Coordinator {
    var childCoordinators =  [Coordinator]()
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let loginView = LoginView.instantiate()
        loginView.loginVm = LoginVM.init(storage: Storage(), delegate: loginView)
        loginView.loginVm?.coordinator = self
        navigationController.pushViewController(loginView, animated: false)
    }
    
    func login(user: User) {
        let shoppingListCoordinator = ShoppingListCoordinator(navigationController: navigationController, user: user)
        shoppingListCoordinator.start()
    }
    
    func register() {
        let registerCoordinator = RegisterCoordinator(navigationController: navigationController)
        registerCoordinator.start()
    }
}
