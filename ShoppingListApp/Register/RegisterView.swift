//
//  LoginView.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 26/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class RegisterView: UIViewController, Storyboarded {
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwrodTextField: UITextField!
    @IBOutlet var alertMessage: UILabel!
    @IBOutlet var registerButton: UIButton!
    
    var registerVM: RegisterVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertMessage.isHidden = true
    }
    
    @IBAction func registerButtonPressed(sender: Any) {
        registerVM?.register()
    }
}

extension RegisterView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        alertMessage.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var textFieldText = textField.text
        if string == "" {
            if textFieldText != "" {
                textFieldText!.remove(at: textFieldText!.index(before: textFieldText!.endIndex))
            }
        }
        if textField == usernameTextField {
            registerVM?.updateUsername(updatedUsername: textFieldText! + string)
        } else if textField == passwrodTextField {
            registerVM?.updatePassword(updatedPassword: textFieldText! + string)
        } else {
            registerVM?.updateConfirmPassword(updatedConfirmPassword: textFieldText! + string)
        }
        return true
    }
}

extension RegisterView: RegisterProtocol {
    func registerDidSucced() {
        registerVM?.coordinator?.register()
    }
    
    func invalidInfos() {
        alertMessage.isHidden = false
        alertMessage.text = "Invalid Informations!"
    }
    
    func userExists() {
        alertMessage.isHidden = false
        alertMessage.text = "User already exists!"
    }
}
