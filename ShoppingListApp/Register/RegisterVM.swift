//
//  RegisterVM.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 26/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

protocol RegisterProtocol {
    func registerDidSucced()
    func invalidInfos()
    func userExists()
}

class RegisterVM {
    private var newUsername: String?
    private var newPassword: String?
    private var confirmPassword: String?
    private var storage: StorageProtocol
    var delegate: RegisterProtocol
    var coordinator: RegisterCoordinator?
    
    init(storage: StorageProtocol, delegate: RegisterProtocol) {
        self.storage = storage
        self.delegate = delegate
    }
    
    func getUsername() -> String? {
        return newUsername
    }
    
    func getPassword() -> String? {
        return newPassword
    }
    
    func getConfirmPassword() -> String? {
        return confirmPassword
    }
    
    func updateUsername(updatedUsername: String) {
        newUsername = updatedUsername
    }
    
    func updatePassword(updatedPassword: String) {
        newPassword = updatedPassword
    }
    
    func updateConfirmPassword(updatedConfirmPassword: String) {
        confirmPassword = updatedConfirmPassword
    }
    
    func isUsernameValid() -> Bool {
        if let username = newUsername {
            return username.count > 0
        }
        return false
    }
    
    func isPasswordValid() -> Bool {
        if let password = newPassword {
            if let confirmPassword = confirmPassword {
                return password.count > 0 && confirmPassword.count > 0 && confirmPassword == password
            }
        }
        return false
    }
    
    func findUser() -> User? {
        let usersArray: [User] = storage.readJSONFromFile()
        if usersArray.count == 0 {
            return nil
        } else {
            for user in usersArray {
                if user.getUsername() == newUsername {
                    return user
                }
            }
            return nil
        }
    }
    
    func register() {
        if !(isUsernameValid() && isPasswordValid()) {
            delegate.invalidInfos()
        } else
        if findUser() == nil {
            let newUser = User(username: newUsername!, password: newPassword!)
            storage.writeToJSONFile(user: newUser)
            delegate.registerDidSucced()
        } else {
            delegate.userExists()
        }
    }
}
