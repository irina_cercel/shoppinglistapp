//
//  AddItemView.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 30/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class AddItemView: UIViewController, Storyboarded {
    @IBOutlet private var nameTextField: UITextField!
    @IBOutlet private var descriptionTextField: UITextField!
    @IBOutlet private var alertMessage: UILabel!
    
    var addItemVM: AddItemVM?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertMessage.isHidden = true
    }
    
    @IBAction func submitButtonPressed(sender: Any) {
        addItemVM?.addItem(user: (addItemVM?.getUser())!)
    }
}


extension AddItemView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        alertMessage.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var textFieldText = textField.text
        if string == "" {
            if textFieldText != "" {
                textFieldText!.remove(at: textFieldText!.index(before: textFieldText!.endIndex))
            }
        }
        if textField == nameTextField {
            addItemVM?.setItemName(name: textFieldText! + string)
        } else {
            addItemVM?.setItemDescription(description: textFieldText! + string)
        }
        return true
    }
}

extension AddItemView: AddItemDelegate {
    func addDidSucced() {
        addItemVM?.coordinator?.submit()
    }
    
    func addDidFail() {
        alertMessage.isHidden = false
    }
}
