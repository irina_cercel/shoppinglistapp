//
//  AddItemVM.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 30/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

protocol AddItemDelegate {
    func addDidSucced()
    func addDidFail()
}

class AddItemVM {
    private var delegate: AddItemDelegate
    private var user: User
    private var storage: StorageProtocol
    private var itemName: String?
    private var itemDescription: String?
    var coordinator: AddItemCoordinator?
    
    func getUser() -> User {
        return user
    }
    
    func getItemName() -> String? {
        return itemName
    }
    
    init(storage: StorageProtocol, user: User, delegate: AddItemDelegate) {
        self.storage = storage
        self.user = user
        self.delegate = delegate
    }
    
    func isItemNameValid() -> Bool {
        if let name = itemName {
            print(name)
            return name.count > 0
        }
        return false
    }
    
    func isItemDescValid() -> Bool {
        if let description = itemDescription {
            return description.count > 0
        }
        return false
    }
    
    func setItemName(name: String) {
        itemName = name
    }
    
    func setItemDescription(description: String) {
        itemDescription = description
    }
    
    func addItem(user: User) {
        if !(isItemDescValid() && isItemNameValid()) {
            delegate.addDidFail()
        } else {
            let item = Item(name: itemName!, description: itemDescription!, user: user)
            storage.writeToJSONFile(user: item as Item)
            delegate.addDidSucced()
        }
    }
    
}
