//
//  UserModel.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 26/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

class User: Codable {
    private var username: String
    private var password: String
    
    func getUsername() -> String {
        return username
    }
    
    func getPassword() -> String {
        return password
    }
    
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
}

