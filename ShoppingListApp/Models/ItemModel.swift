//
//  Items.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 30/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

class Item: Codable {
    private var name: String
    private var description: String
    private var user: User
    
    init(name: String, description: String, user: User) {
        self.name = name
        self.description = description
        self.user = user
    }
    
    func getName() -> String {
        return name
    }
    
    func getDescrpition() -> String {
        return description
    }
    
    func getUser() -> User {
        return user
    }
}

class ItemViewModel: MVVMCellItem {
    var item: Item
    var title: String
    var description: String?
    
    init(item: Item) {
        self.item = item
        self.title = item.getName()
        self.description = item.getDescrpition()
    }
}
