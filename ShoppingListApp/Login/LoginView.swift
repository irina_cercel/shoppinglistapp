//
//  LoginView.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 26/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class LoginView: UIViewController, Storyboarded {
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwrodTextField: UITextField!
    @IBOutlet var alertMessage: UILabel!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    
    var loginVm: LoginVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertMessage.isHidden = true
    }
    
    @IBAction func loginButtonPressed(sender: Any) {
        loginVm?.login()
    }
    
    @IBAction func registerButtonPressed(sender: Any) {
        loginVm?.coordinator?.register()
    }
}

extension LoginView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        alertMessage.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var textFieldText = textField.text
        if string == "" {
            if textFieldText != "" {
                textFieldText!.remove(at: textFieldText!.index(before: textFieldText!.endIndex))
            }
        }
        if textField == usernameTextField {
            loginVm?.newUsername = textFieldText! + string
        } else {
            loginVm?.newPassword = textFieldText! + string
        }
        return true
    }
}

extension LoginView: LoginVMDelegate {
    func loginDidSucced(user: User) {
        loginVm?.coordinator?.login(user: user)
    }
    
    func loginDidFail() {
        alertMessage.isHidden = false
    }
}
