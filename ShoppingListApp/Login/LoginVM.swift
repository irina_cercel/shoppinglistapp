//
//  LoginVM.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 26/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

protocol LoginVMDelegate {
    func loginDidSucced(user: User)
    func loginDidFail()
}

class LoginVM {
    var newUsername: String?
    var newPassword: String?
    var storage: StorageProtocol
    var delegate: LoginVMDelegate
    var coordinator: LoginCoordinator?
    
    init(storage: StorageProtocol, delegate: LoginVMDelegate) {
        self.storage = storage
        self.delegate = delegate
    }
    
    func isUsernameValid() -> Bool {
        if let username = newUsername {
            return username.count > 0
        }
        return false
    }
    
    func isPasswordValid() -> Bool {
        if let password = newPassword {
            return password.count > 0
        }
        return false
    }
    
    func findUser() -> User? {
        let usersArray: [User] = storage.readJSONFromFile()
        if usersArray.count == 0 {
            return nil
        } else {
            for user in usersArray {
                if user.getUsername() == newUsername && user.getPassword() == newPassword {
                    return user
                }
            }
            return nil
        }
    }
    
    func login() {
        if !(isPasswordValid() && isUsernameValid()) {
            delegate.loginDidFail()
        } else {
            if let user = findUser() {
                delegate.loginDidSucced(user: user)
            } else {
                delegate.loginDidFail()
            }
        }
    }
}
