//
//  ShoppingListVM.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 29/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

protocol ShoppingListDelegate {
    func addItem()
}

protocol MVVMCellItem {
    var title: String { get set }
    var description: String? { get set }
}

class ActionItem: MVVMCellItem {
    var title: String
    var description: String?
    
    init(title: String) {
        self.title = title
    }
}

class ShoppingListVM {
    private var delegate: ShoppingListDelegate
    private var dataSource: [MVVMCellItem] {
        return shoppingItems + [ActionItem(title: "Delete All")]
    }
    private var shoppingItems: [ItemViewModel]
    private var user: User
    private var storage: StorageProtocol
    var coordinator: ShoppingListCoordinator?

    init(storage: StorageProtocol, user: User, delegate: ShoppingListDelegate) {
        self.storage = storage
        self.user = user
        self.delegate = delegate
        self.shoppingItems = []
        self.setItemsFromJSON()
    }
    
    func setItemsFromJSON(){
        shoppingItems = []
        let allItemsArray: [Item] = storage.readJSONFromFile()
        for item in allItemsArray {
            if item.getUser().getUsername() == user.getUsername() {
                shoppingItems.append(ItemViewModel.init(item: item))
            }
        }
        print(shoppingItems)
    }
    
    func getUser() -> User {
        return user
    }
    
    func cellViewModel(for index: Int) -> MVVMCellItem {
        return dataSource[index]
    }
    
    func getItemsArray() -> [ItemViewModel] {
        return shoppingItems
    }
    
    func addItem() {
        delegate.addItem()
    }
    
    func getNumberOfItems() -> Int {
        return shoppingItems.count
    }
    
    func deleteAll() {
        shoppingItems = []
        var allItemsArray: [Item] = storage.readJSONFromFile()
        allItemsArray.removeAll { $0.getUser().getUsername() == user.getUsername() }
        storage.writeAllToJSONFile(array: allItemsArray)
    }
}
