//
//  ShoppingListView.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 29/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class ShoppingListView: UIViewController {
    @IBOutlet private var shoppingListTableView: UITableView!
    @IBOutlet private var welcomeLabel: UILabel!
    
    var shoppingListVM: ShoppingListVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        welcomeLabel.text = "Welcome " + (shoppingListVM?.getUser().getUsername())!
        shoppingListTableView.register(UINib(nibName: "ItemCell", bundle: nil), forCellReuseIdentifier: "ItemCell")
        shoppingListTableView.register(UINib(nibName: "DeleteCell", bundle: nil), forCellReuseIdentifier: "DeleteCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        shoppingListVM?.setItemsFromJSON()
        shoppingListTableView.reloadData()
    }
    
    @IBAction func addButtonPressed(sender: Any) {
       shoppingListVM?.addItem()
    }
    
    @IBAction func logOutButtonPressed(sender: Any) {
        shoppingListVM?.coordinator?.logOut()
    }
}

extension ShoppingListView: UITableViewDataSource, Storyboarded {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let numberOfRows = shoppingListVM?.getNumberOfItems() {
            return numberOfRows + 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModelCell = shoppingListVM?.cellViewModel(for: indexPath.row)
        let cellIdentifier: String = viewModelCell is ItemViewModel ? "ItemCell": "DeleteCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MVVMCell
        if let cell = cell, let viewModelCell = viewModelCell {
            cell.configure(item: viewModelCell)
            return cell
        }
        return UITableViewCell()
    }
}

extension ShoppingListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let numberOfRows = shoppingListVM?.getNumberOfItems() {
            if indexPath.row == numberOfRows {
                shoppingListVM?.deleteAll()
                tableView.reloadData()
            }
        }
    }
}

extension ShoppingListView: ShoppingListDelegate {
    func addItem() {
        shoppingListVM?.coordinator?.addItem()
    }
}
