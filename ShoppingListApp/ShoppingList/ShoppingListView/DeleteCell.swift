//
//  DeleteCell.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 05/08/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

class DeleteCell: UITableViewCell, MVVMCell {
    func configure(item: MVVMCellItem) {
        actionTitle.text = item.title
    }
    
    @IBOutlet weak var actionTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
