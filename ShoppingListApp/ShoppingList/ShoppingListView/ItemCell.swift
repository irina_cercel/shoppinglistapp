//
//  ItemCell.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 29/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import UIKit

protocol MVVMCell: UITableViewCell {
    func configure(item: MVVMCellItem)
}

class ItemCell: UITableViewCell, MVVMCell {
    
    @IBOutlet private var itemNameLabel: UILabel!
    @IBOutlet private var itemDescriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(item: MVVMCellItem) {
        itemNameLabel.text = item.title
        itemDescriptionLabel.text = item.description
    }
}
