//
//  Storage.swift
//  ShoppingListApp
//
//  Created by Irina Cercel on 26/07/2019.
//  Copyright © 2019 Cercel Irina. All rights reserved.
//

import Foundation

protocol StorageProtocol {
    var jsonFilePath: URL? { get }
    func readJSONFromFile<T: Codable>() -> [T]
    func writeToJSONFile<T: Codable>(user: T)
    func writeAllToJSONFile<T: Codable>(array: [T])
}

class Storage: StorageProtocol {

    var jsonFilePath: URL?
    
    func setjsonFilePath(path: String) {
        let fileManager = FileManager.default
        if let documentsDirectoryPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = documentsDirectoryPath.appendingPathComponent(path)
            jsonFilePath = fileURL
            var isDirectory: ObjCBool = false
            if !fileManager.fileExists(atPath: (fileURL.absoluteString), isDirectory: &isDirectory) {
                let created = fileManager.createFile(atPath: fileURL.absoluteString, contents: nil, attributes: nil)
                if !created {
                    print("not created")
                }
            }
        }
    }
    
    func writeAllToJSONFile<T>(array: [T]) where T : Decodable, T : Encodable {
        if T.self == User.self {
            setjsonFilePath(path: "users.json")
        } else {
            setjsonFilePath(path: "items.json")
        }
        if let jsonFilePath = jsonFilePath {
            do {
                let json = JSONEncoder()
                json.outputFormatting = .prettyPrinted
                let jsonData = try json.encode(array)
                do {
                    try jsonData.write(to: jsonFilePath)
                } catch { print("Error at writing in json file") }
            }
            catch {
                print("Error at encoding data")
            }
        }
    }
    
    func readJSONFromFile<T>() -> [T] where T : Decodable, T : Encodable {
        if T.self == User.self {
            setjsonFilePath(path: "users.json")
        } else {
            setjsonFilePath(path: "items.json")
        }
        if let jsonFilePath = jsonFilePath {
            do {
                let data = try Data(contentsOf: jsonFilePath)
                let jsonArray = try? JSONDecoder().decode([T].self, from: data)
                return jsonArray ?? []
            } catch {
                print("Error at reading from json file")
            }
        }
        return []
    }
    
    func writeToJSONFile<T>(user: T)  where T : Decodable, T : Encodable{
        if T.self == User.self {
            setjsonFilePath(path: "users.json")
        } else {
            setjsonFilePath(path: "items.json")
        }
        if let jsonFilePath = jsonFilePath {
            var usersArray = readJSONFromFile() as [T]
            usersArray.append(user)
            do {
                let json = JSONEncoder()
                json.outputFormatting = .prettyPrinted
                let jsonData = try json.encode(usersArray)
                do {
                    try jsonData.write(to: jsonFilePath)
                } catch { print("Error at writing in json file") }
            }
            catch {
                print("Error at encoding data")
            }
        }
    }
}
